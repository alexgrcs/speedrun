import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import App from '../src/containers/App';

const props = {
  history: {},
  location: {},
  match: {}
}

describe('<App />', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = mount(
      <MemoryRouter>
        <App {...props} />
      </MemoryRouter>
    );
  })

  test('should render without throwing an error', () => {
    expect(wrapper.find('div').exists()).toBe(true)
  })

  test('should render a List component', () => {
    expect(wrapper.find('List').exists()).toBe(true)
  })
})