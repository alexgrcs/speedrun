import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import Preview from '../src/components/Preview';
import previewData from '../__mocks__/preview';

describe('<Preview />', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = mount(
      <MemoryRouter>
        <Preview {...previewData} />
      </MemoryRouter>
    );
  })

  test('should render without throwing an error', () => {
    expect(wrapper.find('div').exists()).toBe(true)
  })

  test('should render a Card component', () => {
    expect(wrapper.find('Card').exists()).toBe(true)
  })

  test('should render an CardContent component', () => {
    expect(wrapper.find('CardContent').exists()).toBe(true)
  })

  test('should render an CardActions component', () => {
    expect(wrapper.find('CardActions').exists()).toBe(true)
  })
})