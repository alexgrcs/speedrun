import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import Item from '../src/components/Item';
import games from '../__mocks__/games';

const itemData = games[0];

describe('<Item />', () => {

  let wrapper;
  beforeEach(() => {
    wrapper = mount(
      <MemoryRouter>
        <Item {...itemData} />
      </MemoryRouter>
    );
  })

  test('should render without throwing an error', () => {
    expect(wrapper.find('div').exists()).toBe(true)
  })

  test('should render a ListItem component', () => {
    expect(wrapper.find('ListItem').exists()).toBe(true)
  })

  test('should render an Avatar component', () => {
    expect(wrapper.find('Avatar').exists()).toBe(true)
  })

  test('should render an ListItemText component', () => {
    expect(wrapper.find('ListItemText').exists()).toBe(true)
  })
})