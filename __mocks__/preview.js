const preview = {
  "date": "2015-10-29",
  "playerId": "qxkl4680",
  "playerName": "Tayler",
  "video": "https://www.youtube.com/watch?v=KnZs6CWldLc",
  "time": "3h 27m 16s 0ms",
  "game": "j1n4296p",
  "id": "j1n4296p",
  "logo": "https://www.speedrun.com/themes/wwe_smackdown_here_comes_the_pain/cover-128.png",
  "cover": "https://www.speedrun.com/themes/wwe_smackdown_here_comes_the_pain/cover-256.png",
  "name": "'WWE Smackdown! Here Comes the Pain"
};

export default preview;