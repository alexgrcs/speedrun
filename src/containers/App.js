import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Item from '../components/Item';
import Preview from '../components/Preview';
import { getGames, getRun } from '../api';

const styles = theme => ({
  root: {
    padding: '100px 30px',
    backgroundColor: theme.palette.grey[300],
  },
  list: {
    maxWidth: 500,
    margin: 'auto',
    backgroundColor: theme.palette.background.paper,
  }
});

class App extends React.Component {
  state = {
    games: null,
    selectedGame: null
  }

  componentDidMount() {
    getGames().then(games => this.setState({ games }));

    this.setPreview();
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.match || !this.props.match.params)
      return false;

    if (this.props.match.params === prevProps.match.params)
      return false;

    this.setPreview();
  }

  setPreview = () => {
    const { match } = this.props;

    if (!match || !match.params || !match.params.game) {
      this.setState({ selectedGame: null });
      return false
    };

    getRun(match.params.game).then(selectedGame => {
      selectedGame ?
        this.setState({ selectedGame }) :
        this.props.history.push('/');
    });
  }

  render() {
    const { classes } = this.props;
    const { games, selectedGame } = this.state;

    return (
      <div className={classes.root}>
        {selectedGame && <Preview {...selectedGame} />}
        <List className={classes.list}>
          {games && games.map(item => <Item key={item.id} {...item} />)}
        </List>
      </div>
    )
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default withStyles(styles)(App);