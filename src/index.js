import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { injectGlobal } from 'styled-components';
import App from './containers/App';

const wrapper = document.getElementById('app');
wrapper && ReactDOM.render(
  <Router>
    <Route path="/:game?" component={App} />
  </Router>,
wrapper);

injectGlobal`
  body {
    margin: 0;
    font-family: "Helvetica Neue", Arial, sans-serif;
    font-size: 16px;
    color: #a2a19f;
    -webkit-font-smoothing: antialiased;
	  -moz-osx-font-smoothing: grayscale;
  }
  h1, h2, h3, p {
    margin: 0 0 1em;
  }
  a {
    text-decoration: none;
  }
`;