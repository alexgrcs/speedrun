import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'fixed',
    top: 0,
    left: 0,
    zIndex: 3,
    width: '100%',
    height: '100%'
  },
  cover: {
    display: 'block',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: -1,
    width: '100%',
    height: '100%',
    background: 'rgba(0,0,0,0.75)',
    cursor: 'pointer'
  },
  card: {
    width: 345,
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  fieldName: {
    opacity: 0.5
  }
};

const Preview = ({ classes, cover, name, playerName, time, video }) => (
  <div className={classes.root}>
    <Link className={classes.cover} to={'/'}></Link>
    <Card className={classes.card}>
      <CardMedia
        className={classes.media}
        image={cover}
        title="Contemplative Reptile"
      />
      <CardContent>
        <Typography gutterBottom variant="headline" component="h2">
          {name}
        </Typography>
        <Typography component="p">
          <span className={classes.fieldName}>Player:</span> {playerName}
        </Typography>
        <Typography component="p">
          <span className={classes.fieldName}>Time:</span> {time}
        </Typography>
      </CardContent>
      <CardActions>
        <a href={video} target="_blank">
          <Button size="small" color="primary">
            Watch video
          </Button>
        </a>
      </CardActions>
    </Card>
  </div>
);

Preview.propTypes = {
  classes: PropTypes.object.isRequired,
  cover: PropTypes.string,
  name: PropTypes.string,
  playerName: PropTypes.string,
  time: PropTypes.string,
  video: PropTypes.string,
};

export default withStyles(styles)(Preview);