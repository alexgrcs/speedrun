import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';

const Item = ({ id, name, logo, classes }) => (
  <Link to={`/${id}`}>
    <ListItem dense button className={classes.listItem}>
      <Avatar alt={name} src={logo} />
      <ListItemText primary={name} />
    </ListItem>
  </Link>
);

Item.propTypes = {
  classes: PropTypes.object.isRequired,
  id: PropTypes.string,
  name: PropTypes.string,
  logo: PropTypes.string
};

export default withStyles(theme => theme)(Item);