/**
 * API utils
 */

import axios from 'axios';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

momentDurationFormatSetup(moment);

const API_URL = 'https://www.speedrun.com/api/v1/';

/** 
 * Speedrun API GET request
 */
function getRequest(params, callback) {
  return axios.get(`${API_URL}${params}`)
    .then(response => {
      const { data } = response;
      return (data && data.data) ? callback(data.data) : null;
    })
    .catch(error => console.log(`Error: ${error.message}`));
}

/**
 * Get games data
 * @param {String} url 
 */
export function getGames() {
  return getRequest('games', parseGamesData);
}

/**
 * Get game run data for detail page
 * @param {String} gameId 
 */
export async function getRun(gameId) {
  let firstRunData = await getGameFirstRun(gameId);
  let gameData = await getGame(firstRunData.game);
  let userData = {};

  if (!firstRunData.playerName)
    userData = await getUser(firstRunData.playerId);

  return { ...firstRunData, ...gameData, ...userData };
}

/**
 * Get game first run data from API
 * @param {String} gameId 
 */
function getGameFirstRun(gameId) {
  return getRequest(`runs?game=${gameId}`, parseFirstRunData);
}

/**
 * Get game data
 * @param {String} gameName 
 */
function getGame(gameName) {
  return getRequest(`games/${gameName}`, parseGameData);
}

/**
 * Get user data
 * @param {String} userId 
 */
function getUser(userId) {
  return getRequest(`users/${userId}`, parseUserData);
}

/**
 * Parse games data
 * @param {Array} games 
 */
function parseGamesData(games) {
  return games.map(game => parseGameData(game));
}

/**
 * Parse game data
 * @param {Object} game 
 */
function parseGameData({ id, assets, names }) {
  const { international, twitch } = names;
  
  return {
    id,
    logo: assets['cover-medium']['uri'],
    cover: assets['cover-large']['uri'],
    name: international || twitch
  }
}

/**
 * Parse game first run data
 * @param {Object} game
 */
function parseFirstRunData([ run ]) {
  const { date, players, videos, times, game } = run;

  return {
    date,
    playerId: players[0].id,
    playerName: players[0].name,
    video: videos.links[0].uri,
    time: moment.duration(times.primary).format('h[h] m[m] s[s] S[ms]'),
    game
  }
}

/**
 * Parse user data
 * @param {Object} user 
 */
function parseUserData({ names }) {
  return { playerName: names.international };
}