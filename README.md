Speedrun Front-End Test
===========

## Setup
Install dependencies
```sh
$ npm install
```

## Development
Run the local webpack-dev-server and autocompile on [http://localhost:8080/](http://localhost:8080/)
```sh
$ npm start
```

## Unit Test
Run the unit tests
```sh
$ npm run test
```

## Deployment
Create an application build
```sh
$ npm run build
```
